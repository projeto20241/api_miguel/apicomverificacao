const cron = require("node-cron");
const cleanUpBooking = require("./services/cleanUpBookingServices");

//Agendamento da limpeza quando elas ja estiverem passado das datas 
// onde colocamos nossa regra de agendamento 
cron.schedule("*/30 * * * * *", async ()=>{
    try{
        await cleanUpBooking();
        console.log("Limpeza automatica executada");
    }catch(error){
        console.error("Erro ao executar limpeza automatica", error);
    }
});
