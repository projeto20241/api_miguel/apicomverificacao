const connect = require("../db/connect");

module.exports = class ScheduleController {
  static async createSchedule(req, res) {
    const { dateStart, dateEnd, days, timeStart, timeEnd, classroom, user } =
      req.body;
 
    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !timeStart ||
      !timeEnd ||
      !classroom ||
      !user
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }

    const dayString = days.join(",");

    try {
      const overlapQuery = `
        SELECT * FROM schedule WHERE classroom = '${classroom}' AND (
          (dateStart <= '${dateEnd}' AND dateEnd >= '${dateStart}') AND
          (timeStart <= '${timeEnd}' AND timeEnd >= '${timeStart}') AND
          (
            days LIKE '%Seg%' AND '${dayString}' LIKE '%Seg%' OR
            days LIKE '%Ter%' AND '${dayString}' LIKE '%Ter%' OR
            days LIKE '%Qua%' AND '${dayString}' LIKE '%Qua%' OR
            days LIKE '%Qui%' AND '${dayString}' LIKE '%Qui%' OR
            days LIKE '%Sex%' AND '${dayString}' LIKE '%Sex%' OR
            days LIKE '%Sab%' AND '${dayString}' LIKE '%Sab%'
          )
        )`;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
          return res
            .status(500)
            .json({ error: "Erro ao verificar agendamento" });
        }

        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "Sala ocupada para os mesmos dias e horários" });
        }

        const insertQuery = `
          INSERT INTO schedule (dateStart, dateEnd, days, user, timeStart, timeEnd, classroom) VALUES (
            '${dateStart}',
            '${dateEnd}',
            '${dayString}',
            '${user}',
            '${timeStart}',
            '${timeEnd}',
            '${classroom}'
          )`;

        connect.query(insertQuery, function (err) {
          if (err) {
            return res
              .status(500)
              .json({ error: "Erro ao cadastrar agendamento" });
          }
          return res
            .status(201)
            .json({ message: "Agendamento cadastrado com sucesso" });
        });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno de servidor" });
    }
  }
};
